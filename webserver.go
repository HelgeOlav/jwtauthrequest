package main

import (
	"bitbucket.org/HelgeOlav/jwtauthrequest/jwtauthapi"
	"fmt"
	"log"
	"net/http"
	"strings"
)

// FindClientByEndpoint return right endpoint
func (r *RootConfig) FindClientByEndpoint(ep string) *jwtauthapi.ClientConfig {
	if r != nil {
		r.mux.RLock()
		defer r.mux.RUnlock()
		for _, client := range r.Clients {
			for _, endpoint := range client.Endpoints {
				if endpoint == ep {
					return &client
				}
			}
		}
	}
	return nil
}

// StartWebServer launches the webserver. This call should never return unless a fatal error occurred.
func (r *RootConfig) StartWebServer() {
	r.Init()
	// start webserver
	http.HandleFunc("/", r.requestHandler)
	log.Println("Listening on", r.ListenPort)
	err := http.ListenAndServe(r.ListenPort, nil)
	log.Println(err)
}

// requestHandler is our HTTP request handler
func (r *RootConfig) requestHandler(w http.ResponseWriter, req *http.Request) {
	// get client
	client := r.FindClientByEndpoint(req.URL.Path)
	if client == nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println("invalid endpoint " + req.URL.Path)
		return
	}
	// get token
	var token string
	if keys, ok := req.URL.Query()[client.TokenName]; ok {
		if len(keys) > 0 {
			token = keys[0]
		}
	} else {
		token = req.Header.Get(client.TokenName)
	}
	// remove any prefix, if configured
	if len(client.TrimPrefix) > 0 {
		token = strings.TrimPrefix(token, client.TrimPrefix)
	}
	// validate the token
	err := client.ValidateJWT(token)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		if client.Debug {
			_, _ = fmt.Fprint(w, err)
		}
		log.Println(err)
		return
	}
}
