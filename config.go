package main

import (
	"bitbucket.org/HelgeOlav/jwtauthrequest/jwtauthapi"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"
)

// RootConfig is the config file structure
type RootConfig struct {
	Clients        []jwtauthapi.ClientConfig `json:"clients"`
	ListenPort     string                    `json:"listen-port"`      // HTTP listen port
	DefaultTTL     int                       `json:"default-ttl"`      // default cache time for JWKS in seconds, use -1 to disable
	MakeDebugToken bool                      `json:"make_debug_token"` // if true startup will print out a debug token
	mux            sync.RWMutex
}

func (rc *RootConfig) SetDefaults() {
	if rc != nil {
		rc.ListenPort = ":8080" // the default listenport
		rc.DefaultTTL = 3600    // 1 hour time-to-live for cached JWK entries
	}
}

// LoadConfigFromFile load a configuration file from disk
func LoadConfigFromFile(fn string) (rc RootConfig, err error) {
	rc.SetDefaults()
	bytes, err := os.ReadFile(fn)
	if err != nil {
		return
	}
	// parse JSON
	err = json.Unmarshal(bytes, &rc)
	return
}

// Init init all endpoints
func (rc *RootConfig) Init() {
	if rc != nil {
		rc.mux.Lock()
		defer rc.mux.Unlock()
		for idx, client := range rc.Clients {
			err := client.Init()
			if err != nil {
				f := fmt.Sprintf("%v: %v\n", client.Name, err)
				log.Println(f)
			}
			// update TTL
			if client.TTL == 0 {
				client.TTL = rc.DefaultTTL
			}
			rc.Clients[idx] = client
		}
	}
}
