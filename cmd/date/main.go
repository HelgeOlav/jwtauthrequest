package main

import (
	"flag"
	"fmt"
	"time"
)

var (
	format = flag.String("format", "2006-02-01", "format for date")
)

func main() {
	flag.Parse()
	fmt.Println(time.Now().Format(*format))
}
