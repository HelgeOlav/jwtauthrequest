@echo on
setlocal
git rev-parse HEAD > hash.txt
go run cmd\date\main.go > date.txt
set /p GIT=<hash.txt
set /p STAMP=<date.txt

go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" .

set GOARCH=amd64
set GOOS=linux
go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" .
endlocal
