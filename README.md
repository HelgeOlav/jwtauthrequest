# Introduction

jwtauthrequest is a backend for nginx [auth_request](http://nginx.org/en/docs/http/ngx_http_auth_request_module.html).
This backend receives tokens from nginx and responds with yes (HTTP response 200) or no (HTTP response 403) for access.

The JWT token is validated against your own configuration.

Features are:

* Supports both HMAC, RSA and ECDSA algorithms.
* For all other than HMAC, validation is done by downloading the JWKS from the issuer.
* For RSA and ECDSA multiple keys and key rotation is supported; the JKWS is refreshed over time and using kid multiple keys can be present.
* Validates iss, exp, iat, aud fields. (kid is also required for RSA and ECDSA.)
* Built on established go libraries; [jwt-go](https://github.com/dgrijalva/jwt-go) and [keyfunc](https://github.com/MicahParks/keyfunc).
* Designed and tested with nginx, but can be used from any application that want to validate a token.

A full-fledged implementation inside nginx is only available when subscribing. Some information about it [here](https://www.nginx.com/blog/authenticating-api-clients-jwt-nginx-plus/). 

## Security notice

* Validating a token correctly is very important. 
* You should make sure that the issuer and audience fields are correct. Unless you specify anything these fields are not verified for corectness.
* A JWT without exp will always be valid, you can change this by adding exp as a required claim. (See the example below.)
* For production, don't use any HMAC algorithms. The shared key has to be, well, shared.
* The JWKS should only be downloaded using https.

## Configuration example

This is a configuration file. For all configuration options look into config.go.

```json
{
  "listen-port": ":8081",
  "clients": [
    {
      "endpoints": [ "/auth" ],
      "name": "Test endpoint",
      "required-claims": [ "exp", "iss" ],
      "allowed-algo": [ "HS256" ],
      "token-name": "Authorization",
      "trim-prefix": "Bearer ",
      "audience": [ "developer" ],
      "HMAC": "qwertyuiopasdfghjklzxcvbnm123456",
      "debug": true,
      "issuer": "golang"
    }
  ]
}
```

A matching nginx configuration (snippet) would be:

```text
location / {
    auth_request /_oauth2_token_introspection;                              
    alias /var/www/html;
}
location = /_oauth2_token_introspection {
    internal;
    proxy_method      GET;
    proxy_pass        http://localhost:8081/auth;
}
```

When you run jwtauthrequest it issues a JWT that you can use with the configuration above. The JWT is valid for one hour after launch.

This configuration assumes that you will authorize using a bearer token.
By default nginx forwards all headers so with a bearer token nothing else needs to be done.
From the configuration you can tweak on token-name
to specify what header (or query name) to get the token from, and trim-prefix is used to remove something in the beginning from the token.

## Calling jwtauthrequest directly

```shell
curl -I http://localhost:8081/auth\?token=<your_token>
```