module bitbucket.org/HelgeOlav/jwtauthrequest

go 1.21

toolchain go1.22.3

require (
	bitbucket.org/HelgeOlav/utils v0.0.0-20241110195852-6fce0408e770
	github.com/MicahParks/keyfunc v1.9.0
	github.com/golang-jwt/jwt/v4 v4.5.1
)
