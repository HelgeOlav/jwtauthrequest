package main

import (
	"bitbucket.org/HelgeOlav/utils"
	"bitbucket.org/HelgeOlav/utils/version"
	"flag"
	"log"
)

func main() {
	ver := version.Get()
	log.Println(ver.LongString())
	// get test token - during initial development
	// start web server
	cf := flag.String("config", "test/testconfig.json", "path to config file")
	flag.Parse()
	var rc RootConfig
	if !utils.ReadConfig([]string{*cf}, "CONFIG", &rc) {
		log.Println("configuration file not loaded")
	}
	if rc.MakeDebugToken {
		token, err := createDummyJWT()
		if err == nil {
			log.Printf("Testtoken: %v\n", token)
		}
	}
	rc.StartWebServer()
}
