package jwtauthapi

import "bitbucket.org/HelgeOlav/utils/version"

const Version = "0.0.11"

func init() {
	version.AddModule(version.ModuleVersion{Name: "bitbucket.org/HelgeOlav/jwtauthrequest/jwtauthapi", Version: Version})
}
