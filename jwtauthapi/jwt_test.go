package jwtauthapi

import (
	"strings"
	"testing"
)

func TestStringIn(t *testing.T) {
	list := []string{"a", "b", "c"}
	// check if match in item in list
	if StringIn("a", &list) == false {
		t.Errorf("failed on item in list")
	}
	// check if match in item not in list
	if StringIn("d", &list) == true {
		t.Errorf("failed on item not in list")
	}
	// check on empty list
	list = []string{}
	if StringIn("a", &list) == true {
		t.Errorf("match on empty list")
	}
	// check on nil list
	if StringIn("a", nil) == true {
		t.Errorf("match on nil list")
	}
}

func TestTrimPrefix(t *testing.T) {
	// check when text should be removed
	input := "Bearer h"
	result := strings.TrimPrefix(input, "Bearer ")
	if result != "h" {
		t.Error("TrimPrefix fail")
	}
	// check when text should not be removed
	input = "h"
	result = strings.TrimPrefix(input, "Bearer ")
	if result != "h" {
		t.Error("TrimPrefix fail")
	}
}

func TestInit(t *testing.T) {
	cc := ClientConfig{HMAC: "abcd"}
	err := cc.Init()
	if err != nil {
		t.Error(err)
		return
	}
	// check default TokenName
	if cc.TokenName != "Authorization" {
		t.Error("TokenName != Authorization")
	}
}
