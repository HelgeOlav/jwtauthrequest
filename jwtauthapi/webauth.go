package jwtauthapi

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

// JwtHandleFunc returns a http.Handler that you can use as your middleware to enforce authentication before reaching
// your application/code.
//
// Use like this:
//
// finalHandler := http.HandlerFunc(final) // this would be your code
//
// mux.Handle("/", cc.JwtHandleFunc(finalHandler))
//
// _ = http.ListenAndServe(":3000", mux)
func (client *ClientConfig) JwtHandleFunc(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		// if used with nil then fail
		if client == nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		// verify endpoints, if any
		if len(client.Endpoints) > 0 {
			isOk := false
			for _, v := range client.Endpoints {
				if v == req.URL.Path {
					isOk = true
					break
				}
			}
			if !isOk {
				w.WriteHeader(http.StatusConflict)
				return
			}
		}
		// check token
		var token string
		if keys, ok := req.URL.Query()[client.TokenName]; ok {
			if len(keys) > 0 {
				token = keys[0]
			}
		} else {
			token = req.Header.Get(client.TokenName)
		}
		if len(client.TrimPrefix) > 0 {
			token = strings.TrimPrefix(token, client.TrimPrefix)
		}
		err := client.ValidateJWT(token)
		if err != nil {
			w.WriteHeader(http.StatusForbidden)
			if client.Debug {
				_, _ = fmt.Fprint(w, err)
			}
			log.Println(err)
			return
		}
		next.ServeHTTP(w, req)
	})
}
