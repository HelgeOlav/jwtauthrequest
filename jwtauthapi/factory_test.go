package jwtauthapi

import (
	"testing"
	"time"
)

var myKeyAsByte = []byte("qwertyuiopasdfghjklzxcvbnm123456")

func TestFactory(t *testing.T) {
	// This is my signing key
	// I make one custom claim here that I require to be present later on
	extras := make(map[string]interface{})
	extras["testing"] = true
	// This creates the token factory with the parameters I configure
	tf := TokenFactory{
		HMAC:       myKeyAsByte,
		Issuer:     "golang",
		ExpireTime: 60,
		Audience:   "developer",
		Extras:     extras,
	}
	// Now get a ClientConfig struct and initialize it, this represents the receiving side
	cc := tf.GetClientConfig()
	err := cc.Init()
	if err != nil {
		t.Error(err)
		return
	}
	// add the testing claim as required
	cc.RequiredClaims = []string{"testing"}
	// issue a certificate
	token, err := tf.Issue()
	if err != nil {
		t.Error(err)
		return
	}
	// validate the certificate
	err = cc.ValidateJWT(token)
	if err != nil {
		t.Error(err)
	}
	// issue an invalid certificate - token expired
	myClaims := make(map[string]interface{})
	myClaims["exp"] = time.Now().Unix() - 3600
	myClaims["iat"] = time.Now().Unix() - 7200
	token, err = tf.IssueWithClaims(myClaims)
	if err != nil {
		t.Error(err)
		return
	}
	// validate the certificate
	err = cc.ValidateJWT(token)
	if err == nil {
		t.Error("Expired token passed validation")
	}
	// issue an invalid certificate - invalid audience
	myClaims = make(map[string]interface{})
	myClaims["aud"] = "webdeveloper"
	token, err = tf.IssueWithClaims(myClaims)
	if err != nil {
		t.Error(err)
		return
	}
	// validate the certificate
	err = cc.ValidateJWT(token)
	if err == nil {
		t.Error("Invalid aud passed validation")
	}
	// issue an invalid certificate - invalid audience
	myClaims = make(map[string]interface{})
	myClaims["iss"] = "C++"
	token, err = tf.IssueWithClaims(myClaims)
	if err != nil {
		t.Error(err)
		return
	}
	// validate the certificate
	err = cc.ValidateJWT(token)
	if err == nil {
		t.Error("Invalid iss passed validation")
	}
	// issue a valid certificate - audience as []string
	myClaims = make(map[string]interface{})
	myClaims["aud"] = []string{tf.Audience}
	token, err = tf.IssueWithClaims(myClaims)
	if err != nil {
		t.Error(err)
		return
	}
	// validate the certificate
	err = cc.ValidateJWT(token)
	if err != nil {
		t.Error("Valid [aud] failed validation")
	}
	// issue a valid certificate - tamper with HMAC
	token, err = tf.Issue()
	if err != nil {
		t.Error(err)
		return
	}
	cc.HMAC = "invalidHMAC"
	// validate the certificate
	err = cc.ValidateJWT(token)
	if err == nil {
		t.Error("Invalid HMAC passed validation")
	}
}
