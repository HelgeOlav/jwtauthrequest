package jwtauthapi

import (
	"bitbucket.org/HelgeOlav/utils"
	"errors"
	"fmt"
	"github.com/MicahParks/keyfunc"
	jwt "github.com/golang-jwt/jwt/v4"
	"log"
	"time"
)

// This file handles validation of JWT

func (c *ClientConfig) Init() (err error) {
	if c == nil {
		return errors.New("nil ClientConfig")
	}
	c.initOnce.Do(func() {
		// set default TokenName
		if len(c.TokenName) == 0 {
			c.TokenName = "Authorization"
		}
		// check key
		if len(c.JWKS) > 0 {
			opts := keyfunc.Options{}
			if c.RefreshUnknownKid {
				t := true
				opts.RefreshUnknownKID = t
			}
			if c.TTL > 0 {
				t := time.Duration(int64(time.Second) * int64(c.TTL))
				opts.RefreshInterval = t
			}
			c.jwks, err = keyfunc.Get(c.JWKS, opts)
			if c.jwks == nil {
				log.Printf("%v failed with %v\n", c.Name, err)
			}
		} else {
			if len(c.HMAC) == 0 {
				log.Printf("%v has no HMAC or JWKS\n", c.Name)
			}
		}
		// check for Algo none
		if len(c.AllowedAlgo) > 0 && utils.IsValueIn("none", &c.AllowedAlgo) {
			log.Printf("%v has algo none, none will always fail\n", c.Name)
		}
	})
	return
}

// StringIn returns true if a is in b
// Deprecated: use utils.IsValueIn instead
func StringIn(a string, b *[]string) bool {
	if b == nil {
		return false
	}
	for _, v := range *b {
		if v == a {
			return true
		}
	}
	return false
}

// ValidateJWT returns an error if token failed validation
func (c *ClientConfig) ValidateJWT(input string) (err error) {
	parser := jwt.NewParser()
	var token *jwt.Token
	// parse token using right keyfunc
	if len(c.JWKS) > 0 {
		token, err = parser.Parse(input, c.jwks.Keyfunc)
	} else {
		token, err = parser.Parse(input, func(_ *jwt.Token) (interface{}, error) {
			return []byte(c.HMAC), nil
		})
	}
	// validate token
	if err == nil {
		if claims, ok := token.Claims.(jwt.MapClaims); ok {
			// check audience
			audRaw := claims["aud"]
			switch audRaw.(type) {
			case string:
				aud := audRaw.(string)
				if len(c.Audience) > 0 && utils.IsValueIn(aud, &c.Audience) == false {
					f := fmt.Sprintf("aud: %v not in %v", aud, c.Audience)
					return errors.New(f)
				}
			case []interface{}:
				matches := 0
				for _, v := range audRaw.([]interface{}) {
					if aud, ok := v.(string); ok {
						if utils.IsValueIn(aud, &c.Audience) {
							matches++
							break
						}
					}
				}
				if matches == 0 {
					f := fmt.Sprintf("aud: %v not in %v", audRaw, c.Audience)
					return errors.New(f)
				}
			default:
				return errors.New("aud: Can't parse")
			}
			// check iss
			if len(c.Issuer) > 0 {
				if iss, ok := claims["iss"].(string); ok {
					if iss != c.Issuer {
						f := fmt.Sprintf("iss: expected %v, got %v", c.Issuer, iss)
						return errors.New(f)
					}
				} else {
					return errors.New("Can't parse iss")
				}
			}
			// check for required claims
			for _, claim := range c.RequiredClaims {
				if _, ok := claims[claim]; ok == false {
					f := fmt.Sprintf("required claim %v not present", claim)
					return errors.New(f)
				}
			}
		} else {
			return errors.New("claims not jwt.MapClaims")
		}
		// check algo
		if len(c.AllowedAlgo) > 0 && utils.IsValueIn(token.Method.Alg(), &c.AllowedAlgo) == false {
			f := fmt.Sprintf("alg: %v not in %v", token.Method.Alg(), c.AllowedAlgo)
			return errors.New(f)
		}
	}
	return
}
