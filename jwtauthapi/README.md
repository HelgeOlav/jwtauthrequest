# jwtauthapi

This is the API you can embed into your own programs to validate incoming JWT.
To use simply create a config struct with the settings you want.

If you specify a jwks URL to download from a background thread will be started to regularly
update this file.

```json
  {
    "endpoints": [ "/endpoint1", "/endpoint2"],
    "name": "Test 1 endpoint",
    "jwks": "http://static.helge.net/test/jwks.json",
    "allowed-algo": [ "RS256" ],
    "audience": [ "aud1", "aud2" ],
    "issuer": "golang"
}
```

Make sure that you specify issuer and audice so these fields can be validated.

In your code, use this as:

```go
cc, err := jwtauthapi.LoadJsonFromFile("filename")
if err != nil {
	panic(err)
}
cc.Init()
// now check a JWT
err = cc.ValidateJWT(myjwttovalidate)
// if err != nil then validation failed and the reason is in the error code
```

## http.Handle middelware

You can also use this library as a HTTP middleware so invalid requests are rejected before it reaches your code.

```go
finalHandler := http.HandlerFunc(final) // this would be your code
mux.Handle("/", cc.JwtHandleFunc(finalHandler))
_ = http.ListenAndServe(":3000", mux)
```

your code in "final" will only be called if a valid token is presented.

## During development

For initial development there is a small factory that you can use to issue and validate your own
certificates. The JWT are signed using HA256 and with a shared secret that you specify. Look at factory_test.go for usage.