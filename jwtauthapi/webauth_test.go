package jwtauthapi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
	"time"
)

// MyTestWebHandlerResult is a struct passed for testing the handler
type MyTestWebHandlerResult struct {
	Time    time.Time `json:"time"`
	Message string    `json:"message"`
	OK      bool      `json:"ok"`
}

// myTestWebHandler is used to serve some content back for testing
func myTestWebHandler(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	result := MyTestWebHandlerResult{
		Time:    time.Now(),
		Message: "Today is a good day for testing this handler!",
		OK:      true,
	}
	outBytes, _ := json.Marshal(&result)
	w.Write(outBytes)
	fmt.Println("myTestWebHandler: hit on", req.URL.Path)
}

func TestClientConfig_JwtHandleFunc(t *testing.T) {
	const webPort = ":9999"
	const authHeader = "X-Token"
	// We use a TokenFactory to validate our requests
	tf := TokenFactory{
		HMAC:       myKeyAsByte,
		Issuer:     "golang",
		ExpireTime: 60,
		Audience:   "developer",
		Extras:     nil,
	}
	// make a ClientConfig so we can launch a webserver
	cc := tf.GetClientConfig()
	cc.Debug = true
	cc.TokenName = authHeader
	cc.Endpoints = []string{"/auth"}
	mux := http.NewServeMux()
	finalHandler := http.HandlerFunc(myTestWebHandler)
	mux.HandleFunc("/unauth", myTestWebHandler)         // just to get the result anyway
	mux.Handle("/auth", cc.JwtHandleFunc(finalHandler)) // this should work
	mux.Handle("/fail", cc.JwtHandleFunc(finalHandler)) // this should fail, not in Endpoints list
	go http.ListenAndServe(webPort, mux)                // launch in background
	time.Sleep(50 * time.Millisecond)
	// check if it is listening
	resp, err := http.Get("http://127.0.0.1" + webPort)
	if err != nil {
		t.Error(err)
		return
	}
	if resp.StatusCode != http.StatusNotFound {
		t.Error("Not my webserver here, quiting")
		return
	}
	// try without auth
	authEndPoint := "http://127.0.0.1" + webPort + "/auth"
	resp, err = http.Get(authEndPoint)
	if err != nil || resp.StatusCode != http.StatusForbidden {
		t.Error("Did not fail on unauth request to /auth")
	}
	// try with auth
	token, _ := tf.Issue()
	client := http.Client{}
	req, _ := http.NewRequest(http.MethodGet, authEndPoint, nil)
	req.Header.Set(authHeader, token)
	resp, err = client.Do(req)
	if err != nil || resp.StatusCode != http.StatusOK {
		t.Error("Failed on auth request to /auth")
		return
	}
	var result MyTestWebHandlerResult
	err = json.NewDecoder(resp.Body).Decode(&result)
	defer resp.Body.Close()
	if err != nil {
		t.Error("Could not decode body from authenticated request to /auth", err)
		return
	}
	if !result.OK {
		t.Error("Invalid result from call, got", result)
	}
	// try from /fail with endpoints
	authEndPoint = "http://127.0.0.1" + webPort + "/fail"
	req, _ = http.NewRequest(http.MethodGet, authEndPoint, nil)
	req.Header.Set(authHeader, token)
	resp, err = client.Do(req)
	if err != nil || resp.StatusCode != http.StatusConflict {
		t.Error("Fail to /fail with endpoints configured")
	}
	// try from /fail without endpoints
	cc.Endpoints = []string{}
	resp, err = client.Do(req)
	if err != nil || resp.StatusCode != http.StatusOK {
		t.Error("Fail to /fail without endpoints")
	}
}
