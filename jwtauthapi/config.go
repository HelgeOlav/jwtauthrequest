package jwtauthapi

import (
	"encoding/json"
	"github.com/MicahParks/keyfunc"
	"os"
	"sync"
)

// ClientConfig describes one specific client configuration endpoint
type ClientConfig struct {
	initOnce          sync.Once     // make sure init is run only once
	Name              string        `json:"name" `               // descriptive name for this client
	JWKS              string        `json:"jwks"`                // URL to download the key set
	HMAC              string        `json:"hmac"`                // HMAC for HS* algorithms, only used if JWKS is empty
	AllowedAlgo       []string      `json:"allowed-algo"`        // list of allowed signature algorithms - if empty any is allowed
	Audience          []string      `json:"audience"`            // if this list is not empty, the JWT audience must match one in this list
	Issuer            string        `json:"issuer"`              // if not empty, this field must match iss in the JWT
	jwks              *keyfunc.JWKS `json:"-"`                   // loaded keys-set
	RequiredClaims    []string      `json:"required-claims"`     // claims that must be present in the JWT
	TTL               int           `json:"ttl"`                 // cache jwks TTL; -1 = disable, 0 = use default, otherwise seconds between each refresh
	RefreshUnknownKid bool          `json:"refresh-unknown-kid"` // if true an unknown KID will attempt a new refresh of the jwkt
	// The properties below are only used by the web server in jwtauthrequest and http.Handle in jwtauthapi
	Debug      bool     `json:"debug,omitempty"`       // if true the reason for failure will be attached in the body as plain text, only used from the main program
	TrimPrefix string   `json:"trim-prefix,omitempty"` // prefix to be removed from token string before processing (like bearer), only used from the main program
	TokenName  string   `json:"token-name,omitempty"`  // name of header or query variable to get token from, only used from the main program
	Endpoints  []string `json:"endpoints,omitempty"`   // paths on this webserver that this client should accept - if empty http.Handle in jwtauthapi will accept all paths
}

// LoadJsonFromFile reads a ClientConfig JSON formatted file from disk
func LoadJsonFromFile(fn string) (cc *ClientConfig, err error) {
	cc = new(ClientConfig)
	// read file from disk
	bytes, err := os.ReadFile(fn)
	// parse json if file is read
	if err == nil {
		err = json.Unmarshal(bytes, cc)
	}
	return
}
