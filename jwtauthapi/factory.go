package jwtauthapi

import (
	"errors"
	jwt "github.com/golang-jwt/jwt/v4"
	"time"
)

// This factory represents a HS256 signer that you can use when starting out. HS256 needs the secret key on both sides to work.

// TokenFactory represents the configuration needed to issue now tokens using HS256
type TokenFactory struct {
	HMAC       []byte                 `json:"hmac"`   // shared secret
	Issuer     string                 `json:"iss"`    // issuer of token
	ExpireTime int64                  `json:"exp"`    // time to add to JWT expiration in seconds
	Audience   string                 `json:"aud"`    // audience field
	Extras     map[string]interface{} `json:"claims"` // extra fields to add to the claim
}

// GetClientConfig returns a client config from the factory
func (t *TokenFactory) GetClientConfig() ClientConfig {
	if t == nil {
		return ClientConfig{}
	}
	// return a good ClientConfig
	cc := ClientConfig{
		HMAC:        string(t.HMAC),
		AllowedAlgo: []string{"HS256"},
		Audience:    []string{t.Audience},
		Issuer:      t.Issuer,
	}
	return cc
}

// Issue issues a new token based on the current token factory
func (t *TokenFactory) Issue() (string, error) {
	return t.IssueWithClaims(nil)
}

// IssueWithClaims issues a new token based on the current token factory with customClaims added last allowing them to override standard claims if you want.
func (t *TokenFactory) IssueWithClaims(customClaims map[string]interface{}) (string, error) {
	if t == nil {
		return "", errors.New("nil TokenFactory")
	}
	token := jwt.New(jwt.SigningMethodHS256)
	claims := jwt.MapClaims{}
	// add any custom claims from the factory if they exist
	for c, v := range t.Extras {
		claims[c] = v
	}
	// set standard claims
	curTime := time.Now().Unix()
	claims["aud"] = t.Audience
	claims["iss"] = t.Issuer
	claims["iat"] = curTime
	claims["exp"] = curTime + t.ExpireTime
	token.Claims = claims
	// add custom claims from callee
	for c, v := range customClaims {
		claims[c] = v
	}
	// sign the new token
	return token.SignedString(t.HMAC)
}
