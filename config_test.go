package main

import (
	"testing"
)

func TestLoadConfigFromFile(t *testing.T) {
	// try load the config file
	rc, err := LoadConfigFromFile("test/testconfig.json")
	if err != nil {
		t.Error("Load config failed", err)
	}
	// validate some content
	if rc.DefaultTTL != 300 {
		t.Errorf("Default TTL parsed wrong, wanted %v got %v", 300, rc.DefaultTTL)
	}

	if len(rc.Clients) != 2 {
		t.Errorf("Wanted 2 client, got %v", len(rc.Clients))
	}
}
