package main

import (
	"bitbucket.org/HelgeOlav/jwtauthrequest/jwtauthapi"
	"bitbucket.org/HelgeOlav/utils/version"
)

func init() {
	version.NAME = "jwtauthrequest"
	version.VERSION = jwtauthapi.Version
}
